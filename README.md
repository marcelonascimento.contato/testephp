# Teste de PHP

Welcome to the Source.

## Setup de Teste

### Primeiros passos

É necessário instalar o Docker e docker-compose que sejam compatíveis com a versão 3 do docker-compose.

Clone o repositorio

```
https://gitlab.com/marcelonascimento.contato/testephp
```

Acesse a pasta Laradock dentro do projeto clonado. 

```
cd testephp/laradock
```

Rode o comando para subir os serviços necessarios para a aplicação funcionar

```
docker-compose up -d nginx mysql phpmyadmin
```

#### Entrando no container e instalando a aplicação


Para entrar no container docker da aplicação rode o comando
```
docker exec -it laradock_workspace_1 bash
```

acesse o phpmyadmin e crie um schema com o nome de myapp

```
url: http://localhost:8081/ 
servidor: laradock_mysql_1
user: root
senha: root
```

Dentro do container instale as dependencias do PHP
```
composer install
```

Após instalado as dependencias e criado o banco de dados rode o comando abaixo para subir as migrations
```
php artisan migrate
``` 

Após isso a aplicação estará instalada e pronta para rodar.
```
url: http://localhost
```